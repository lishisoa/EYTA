//
// Created by Leandro Ishi Soares de Lima on 17/04/17.
//

#ifndef KSGATB_BRANCHINGSTRATEGY_H
#define KSGATB_BRANCHINGSTRATEGY_H

#include "Strategy.h"

//commented out
//namespace EnhanceTranscriptome {
//
//    class BranchingStrategy : public Strategy {
//    private:
//        const map<pair<int, char>, int> &labelToIndex;
//
//    public:
//        void findAlternativePath(FilteredGraph &graph, graph_t &unfilteredGraph,
//                                                    SubgraphVertexFilter *subgraphVertexFilter,
//                                                    const Vertex &v, vector<Vertex> &alternativePath, int iteration,
//                                                    const set<Vertex> &targetNodes, int maxLengthPath, int branches,
//                                                    const MappingInfo &mappingInfo, int startIndex, bool *firstBubble,
//                                                     int &nbOfAlternativePaths, int maxLengthOfAnAlternativeTranscript,
//                                                     int maxBranches,
//                                                    BubbleOutputter &bubbleOutputter);
//
//
//        virtual void processCluster(const vector<MappingInfo> &mappingInfo, FilteredGraph &graph, graph_t &unfilteredGraph,
//                               SubgraphVertexFilter *subgraphVertexFilter, int maxLengthOfAnAlternativeTranscript,
//                               int maxBranches, BubbleOutputter &bubbleOutputter, int cluster, int readId=-1);
//
//        BranchingStrategy(int numberOfNodes, const UnitigLinkingGraph &ulg, const map<pair<int, char>, int> &labelToIndex) : Strategy(numberOfNodes, ulg),
//                                                                                                                                  labelToIndex(labelToIndex) { }
//    };
//
//}


#endif //KSGATB_BRANCHINGSTRATEGY_H
