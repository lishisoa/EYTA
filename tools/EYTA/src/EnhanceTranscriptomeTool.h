//
// Created by Leandro Ishi Soares de Lima on 07/02/17.
//

#ifndef KSGATB_ENHANCETRANSCRIPTOMETOOL_H
#define KSGATB_ENHANCETRANSCRIPTOMETOOL_H

/********************************************************************************/
#include <gatb/gatb_core.hpp>
#include "global.h"
#include "EnhanceTranscriptome.h"
/********************************************************************************/



////////////////////////////////////////////////////////////////////////////////
//
// THIS FILE IS AUTOMATICALLY GENERATED...
//
// THIS IS A SIMPLE EXAMPLE HOW TO USE THE Tool CLASS. IF YOU WANT MORE FEATURES,
// YOU CAN HAVE A LOOK AT THE ToyTool SNIPPET HERE:
//
//      http://gatb-core.gforge.inria.fr/snippets_tools.html
//
////////////////////////////////////////////////////////////////////////////////

class EnhanceTranscriptomeTool : public Tool
{
private:
    void checkParameters();

public:
    // Constructor
    EnhanceTranscriptomeTool () : Tool ("enhance_transcriptome") {
        populateParser(this);
    }

    // Actual job done by the tool is here
    void execute () {
        checkParameters();
        string prefix = getInput()->getStr(STR_PAR_GRAPH_PREFIX);
        int kmerSize = getInput()->getInt(STR_PAR_K);
        int maxLengthAS = getInput()->getInt(STR_PAR_MAX_LENGTH_AS);
        int maxLengthIntron = getInput()->getInt(STR_PAR_MAX_LENGTH_INTRON);
        double editDistance = getInput()->getDouble(STR_PAR_MIN_EDIT_DISTANCE);
        int splicingComplexityAS = getInput()->getInt(STR_AS_SPLICING_COMPLEXITY);
        int splicingComplexityIR = getInput()->getInt(STR_IR_SPLICING_COMPLEXITY);
        int nbCores = getInput()->getInt(STR_PAR_NB_CORES);
        bool outputContext =  getInput()->get(STR_OUTPUT_CONTEXT) != 0;

        EnhanceTranscriptome::EnhanceTranscriptome(prefix + "_merged",
                                                   prefix + ".EYTA.alternative_paths", kmerSize,
                                                   maxLengthAS,
                                                   maxLengthIntron,
                                                   editDistance,
                                                   splicingComplexityAS,
                                                   splicingComplexityIR,
                                                   outputContext,
                                                   nbCores);

        stats.printStatsToFile(prefix + "_final_stats");
    }
};

/********************************************************************************/

#endif //KSGATB_ENHANCETRANSCRIPTOMETOOL_H
