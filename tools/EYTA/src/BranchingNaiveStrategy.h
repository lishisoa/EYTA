//
// Created by Leandro Ishi Soares de Lima on 24/04/17.
//

#ifndef KSGATB_BRANCHINGNAIVESTRATEGY_H
#define KSGATB_BRANCHINGNAIVESTRATEGY_H

#include "Strategy.h"

//commented out
//namespace EnhanceTranscriptome {
//    class BranchingNaiveStrategy : public Strategy {
//    public:
//        void processCluster(const vector<MappingInfo> &mappingInfoCluster, FilteredGraph &graph, graph_t &unfilteredGraph,
//                            SubgraphVertexFilter *subgraphVertexFilter, int maxLengthOfAnAlternativeTranscript,
//                            int maxBranches, BubbleOutputter &bubbleOutputter, int cluster, int readId = -1);
//
//        //constructor inheritance
//        //This inherits all constructors of Strategy
//        using Strategy::Strategy;
//    };
//}


#endif //KSGATB_BRANCHINGNAIVESTRATEGY_H
